let http = require("http")
let port = 4000
let courses = ['HTML', 'CSS', 'NODE.JS', 'BOOTSTRAP', 'JAVASCRIPT']

const server = http.createServer((req, res) => {
    if (req.url == '/') {
        res.writeHead(200, { 'Content-Type': 'text/plain' })
        res.end('Welcome to Booking System')
    }
    if (req.url == '/profile') {
        res.writeHead(200, { 'Content-Type': 'text/plain' })
        res.end('Welcome to your profile!')
    }
    // GET
    if (req.url == '/courses' && req.method == 'GET') {
        res.writeHead(200, { 'Content-Type': 'application/json' })
        res.end("Here's our courses available")
    }
    // POST
    if (req.url == '/addcourse' && req.method == 'POST') {
        res.writeHead(200, { 'Content-Type': 'text/plain' })
        res.end('Add a course to our resources')
    }
    // PUT
    if (req.url == '/updatecourse' && req.method == 'PUT') {
        res.writeHead(200, { 'Content-Type': 'text/plain' })
        res.end('Update a course in our resources')
    }
    // DELETE
    if (req.url == '/archivecourse' && req.method == 'DELETE') {
        res.writeHead(200, { 'Content-Type': 'text/plain' })
        res.end('Archive courses to our resources')
    }
})
server.listen(port);
console.log("Server is running at local host: " + port)